import csv
import numpy as np
import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, Flatten, Dense, Activation, Dropout, MaxPooling2D
from keras.activations import relu
from keras.optimizers import Adam

import cv2
import dlib
from keras.models import load_model
from scipy.spatial import distance as dist
from imutils import face_utils

import os

from common import cnnPreprocess, cropEyes, makeModel
from constants import *


def read_dataset_description(open_eyes=True):
    eyes = {}

    description_file = 'EyeCoordinatesInfo_OpenFace.txt' if open_eyes else 'EyeCoordinatesInfo_ClosedFace.txt'
    with open(os.path.join(datasets_path, description_file)) as file:
        for line in file:
            items = line.strip().split(' ')
            filename = items[0]
            coords = [int(i) for i in items[1:]]
            eyes[filename] = coords

    return eyes


def train_by_detecting():
    model = makeModel(height, width)

    x_train = np.empty([0, 26, 34, 1])
    y_train = np.empty([0, 1])

    no_eye_detections = set()

    for filename in os.listdir(datasets_path):
        if filename.endswith('.jpg'):
            img = cv2.imread(os.path.join(datasets_path, filename), 0)
        else:
            continue

        # detect eyes
        eyes = cropEyes(img)

        if eyes is None:
            no_eye_detections.add(filename)
            print("None eye found for", filename)
            continue
        else:
            print("Eye found for", filename)
            left_eye, right_eye = eyes

        # label if eye is closed or not
        if filename.startswith('close'):
            label = np.array([[0]])
        else:
            label = np.array([[1]])

        x_train = np.append(x_train, cnnPreprocess(left_eye), axis=0)
        y_train = np.append(y_train, label, axis=0)

    print('x_train: ', x_train.shape)
    print('y_train: ', y_train.shape)
    model.fit(x_train, y_train, batch_size=32, epochs=50)

    model.save(os.path.join(assets_path, 'datasetModel.hdf5'))

    return no_eye_detections


def test_detection_invalidation(no_eye_detections, open_eyes=True):
    # load cnn model
    model = load_model(os.path.join(assets_path, 'datasetModel.hdf5'))

    # load images
    for filename in os.listdir(datasets_path):
        if filename not in no_eye_detections:
            continue

        # detect eyes
        eyes = read_dataset_description(open_eyes)
        x_window = 13
        y_window = 17
        for key, value in eyes.items():
            img = cv2.imread(os.path.join(datasets_path, key), 0)

            lx = int(value[0] - x_window*1)
            lx_w = int(value[0] + x_window*1)
            ly = int(value[1] - y_window*1)
            ly_h = int(value[1] + y_window*1)
            left_cropped = img[ly:ly_h, lx:lx_w]
            right_cropped = img[value[3]-y_window:value[3]+y_window, value[2]-x_window:value[2]+x_window]

            # cv2.imshow('cropped', left_cropped)
            # cv2.waitKey(0)

            lprediction = model.predict(cnnPreprocess(left_cropped.T))
            rprediction = model.predict(cnnPreprocess(right_cropped.T))

            print(key, ', l: ', lprediction, ', r: ', rprediction)

def main():
    no_eye_detections = train_by_detecting()

    print('Checking open eyes')
    print('------------------------------')
    test_detection_invalidation(no_eye_detections, open_eyes=True)

    print('Checking close eyes')
    print('------------------------------')
    test_detection_invalidation(no_eye_detections, open_eyes=False)

if __name__ == '__main__':
    main()