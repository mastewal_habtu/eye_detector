import csv
import numpy as np
import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, Flatten, Dense, Activation, Dropout, MaxPooling2D
from keras.activations import relu
from keras.optimizers import Adam

import cv2
import dlib
from keras.models import load_model
from scipy.spatial import distance as dist
from imutils import face_utils

import os

from common import makeModel, cnnPreprocess, cropEyes
from constants import *


def readCsv(path):
    with open(path, 'r') as f:
        # read the scv file with the dictionary format
        reader = csv.DictReader(f)
        rows = list(reader)

    # imgs is a numpy array with all the images
    # tgs is a numpy array with the tags of the images
    imgs = np.empty((len(list(rows)), height, width, 1), dtype=np.uint8)
    tgs = np.empty((len(list(rows)), 1))

    for row, i in zip(rows, range(len(rows))):

        # convert the list back to the image format
        img = row['image']
        img = img.strip('[').strip(']').split(', ')
        im = np.array(img, dtype=np.uint8)
        im = im.reshape((26, 34))
        im = np.expand_dims(im, axis=2)
        imgs[i] = im

        # the tag for open is 1 and for close is 0
        tag = row['state']
        if tag == 'open':
            tgs[i] = 1
        else:
            tgs[i] = 0

    # shuffle the dataset
    index = np.random.permutation(imgs.shape[0])
    imgs = imgs[index]
    tgs = tgs[index]

    # return images and their respective tags
    return imgs, tgs


def simple_train():
    xTrain, yTrain = readCsv('dataset.csv')

    # scale the values of the images between 0 and 1
    xTrain = xTrain.astype('float32')
    xTrain /= 255

    model = makeModel(height, width)

    # do some data augmentation
    datagen = ImageDataGenerator(
        rotation_range=10,
        width_shift_range=0.2,
        height_shift_range=0.2,
    )
    datagen.fit(xTrain)

    # train the model
    model.fit_generator(datagen.flow(xTrain, yTrain, batch_size=32),
                        steps_per_epoch=len(xTrain) / 32, epochs=50)

    # save the model
    model.save(os.path.join(models_path, 'simpleModel.hdf5'))

def main():
    # load cnn model
    model = load_model(os.path.join(models_path, 'datasetModel.hdf5'))

    # load images
    for filename in os.listdir(datasets_path):
        img = cv2.imread(os.path.join(datasets_path, filename), 0)

        # detect eyes
        eyes = cropEyes(img)

        if eyes is None:
            print("None eye found for", filename)
            continue
        else:
            left_eye, right_eye = eyes

        lprediction = model.predict(cnnPreprocess(left_eye))
        rprediction = model.predict(cnnPreprocess(right_eye))

        print(filename, ', l: ', lprediction, ', r: ', rprediction)


if __name__ == '__main__':
    main()