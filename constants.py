import cv2
import os
import dlib

assets_path = 'assets'
models_path = 'models'
datasets_path = '/home/yehualashet/Downloads/dataset facial'

# we will use images 26x34x1 (1 is for grayscale images)
height = 26
width = 34
dims = 1

face_cascade = cv2.CascadeClassifier(os.path.join(assets_path, 'haarcascade_frontalface_alt.xml'))


predictor = dlib.shape_predictor(os.path.join(assets_path, "shape_predictor_68_face_landmarks.dat"))